from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
import urllib.request, json
import requests

# Create your views here.
def story8_index(request):
    return render(request, 'story8/index.html', {})

def get_book_list_JSON(request, value):
    data = requests.get("https://www.googleapis.com/books/v1/volumes?q="+value).json()
    return JsonResponse(data)
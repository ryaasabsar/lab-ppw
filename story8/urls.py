from django.urls import path, include
from .views import story8_index, get_book_list_JSON

app_name = "story8"

urlpatterns = [
    path('', story8_index, name='story8_index'),
    path('<str:value>' , get_book_list_JSON, name = "booklist"),
]
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import story8.views
# Create your tests here.

class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_not_exists(self):
        response = self.client.get("/story8/naruto/booklist")
        self.assertEqual(response.status_code,404)
    
    def test_url_exists(self):
        response = self.client.get("/story8/")
        self.assertEqual(response.status_code,200)
    
class ViewsTest(TestCase):
    def test_page_using_index_views(self):
        response = resolve("/story8/")
        self.assertEqual(response.func,story8.views.story8_index)
    
    def test_page_using_AJAX_API(self):
        response = resolve("/story8/naruto")
        self.assertEqual(response.func,story8.views.get_book_list_JSON)


class TemplateTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_html_status_page_exist(self):
        response = self.client.get("/story8/")
        self.assertTemplateUsed(response,'story8/index.html')
    
    def test_html_halo_apa_kabar_exist(self):
        response = self.client.get("/story8/")
        self.assertIn("Mini Book Search", response.content.decode())

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium_temp = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')

    def tearDown(self):
        self.selenium_temp.quit()
        super().tearDown()

    def test_title_is_correct(self):
        self.selenium_temp.get(self.live_server_url+"/story8/")
        self.assertIn("book search",self.selenium_temp.title)

    def test_text_is_exist(self):
        self.selenium_temp.get(self.live_server_url+"/story8/")
        self.assertIn("Mini Book Search",self.selenium_temp.page_source)

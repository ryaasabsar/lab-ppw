$(document).ready(function(){
    $("#search-button").click(function(){
        var inputValue = $("#user-input").val()
        var dataRes = ""
        console.log(inputValue)

        $.ajax({
            url : "https://www.googleapis.com/books/v1/volumes?q=" + inputValue,
            error: function(){
                alert("Something went wrong, try again.")
            },
            success: function(data){
                console.log(data)
                
                $('#book-result').empty();
                if(typeof (data.items) === 'undefined'){
                    alert("No book found, try another keyword");
                }

                for(var counter=0; counter<data.items.length; counter++){
                    var number = counter+1;
                    dataRes += '<tr>';
                    dataRes += '<td>' + number + '</td>';
                    dataRes += '<td>' + data.items[counter].volumeInfo.title + '</td>';
                    
                    dataRes += '<td>';
                    if((typeof(data.items[counter].volumeInfo.authors)) === 'undefined'){
                        dataRes += 'No Author'
                    } else{
                        if(data.items[counter].volumeInfo.authors.length == 1){
                            dataRes += data.items[counter].volumeInfo.authors[0]
                        } else{
                            dataRes += '<ul>';
                            for(var j=0; j<data.items[counter].volumeInfo.authors.length; j++){
                                dataRes += '<li>' + data.items[counter].volumeInfo.authors[j] + '</li>';
                            }
                            dataRes += '</ul>';
                        }
                    }
                    dataRes += '</td>'

                    if((typeof(data.items[counter].volumeInfo.imageLinks)) === 'undefined'){
                        dataRes += "<td> No Thumbnail </td>";   
                    } else{
                        dataRes += '<td>' + '<img src=' + data.items[counter].volumeInfo.imageLinks.thumbnail + "> </td>";
                    }
                    dataRes += '</tr>';

                }
                console.log(dataRes)
                $("#book-result").append(dataRes)
            },

        })
    })
})
from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

from .forms import LoginForm, RegisterForm

# Create your views here
def story9_index(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            input_username = form.cleaned_data["username"]
            input_password = form.cleaned_data["password"]
            user_test = authenticate(request, username = input_username, password = input_password)
            if user_test is not None:
                login(request, user_test)
        return redirect("/story9/")
    form = LoginForm()
    response = {
        "form" : form
    }
    return render(request, "story9/index.html", response)

def logout_story9(request):
    logout(request)
    return redirect("/story9/")

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if (form.is_valid()):
            input_username = form.cleaned_data["username"]
            input_email = form.cleaned_data["email"]
            input_password = form.cleaned_data["password"]
            try:
                user_temp = User.objects.get(username = input_username)
                return redirect("/story9/register/")
            except User.DoesNotExist:
                user_temp = User.objects.create_user(input_username, input_email, input_password)
                return redirect("/story9/")
    form = RegisterForm()
    response = {
        "form" : form
    }
    return render(request, "story9/register.html", response)
from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.auth.models import User
from django.urls import resolve, reverse
from django.utils import timezone

from . import views

class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_not_exists(self):
        response = self.client.get("/story9/hello")
        self.assertEqual(response.status_code,404)

        response = self.client.get("/story9/login")
        self.assertEqual(response.status_code,404)
    
    def test_url_exists(self):
        response = self.client.get("/story9/")
        self.assertEqual(response.status_code,200)
        
        response = self.client.get("/story9/register/")
        self.assertEqual(response.status_code,200)

        response = self.client.get("/story9/logout/")
        self.assertEqual(response.status_code,302)
    
class ViewsTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('nanda', 'nanda', 'nanda1212')
        self.user.save()

    def test_page_using_correct_views_method(self):
        response = resolve("/story9/")
        self.assertEqual(response.func,views.story9_index)

        response = resolve("/story9/register/")
        self.assertEqual(response.func,views.register)

        response = resolve("/story9/logout/")
        self.assertEqual(response.func,views.logout_story9)

    def test_login_test(self):
        response = self.client.post('/story9/', data={'username' : 'nanda', 'password' : 'nanda1212'})
        self.assertRedirects(response, '/story9/')
    
    def test_register_test(self):
        response = self.client.post('/story9/register/', data={'username' : 'nanda12', 'email':'nanda', 'password' : 'nanda121212'})
        self.assertEqual(response.status_code,302)

    def test_register_already_exist_test(self):
        response = self.client.post('/story9/register/', data={'username' : 'nanda', 'email':'nanda', 'password' : 'nanda121212'})
        self.assertEqual(response.status_code,302)


class TemplateTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_html_login_page_exist(self):
        response = self.client.get("/story9/")
        self.assertTemplateUsed(response,'story9/index.html')
    
    def test_html_masuk_kang_exist(self):
        response = self.client.get("/story9/")
        self.assertIn("Masuk Kang", response.content.decode())

from django.urls import path, include
from .views import status

app_name = "story6"

urlpatterns = [
    path('', status, name='status'),
]
from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    status_message = forms.CharField(widget=forms.Textarea(attrs={'cols': 10, 'rows': 20}))
    class Meta:
        model = Status
        fields = ['status_message']

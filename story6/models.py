from django.db import models

# Create your models here.

class Status(models.Model):
    status_message = models.CharField(max_length=300)
    created_time = models.DateTimeField(auto_now_add=True)
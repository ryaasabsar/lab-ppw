from django.shortcuts import render, HttpResponse, redirect
from .forms import StatusForm
from .models import Status

# Create your views here.
def status(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story6:status')
    else:
        form = StatusForm()

    response = {
        'form': form,
        'all_status': Status.objects.all().order_by('-id'),
    }

    return render(request, 'story6/index.html', response)

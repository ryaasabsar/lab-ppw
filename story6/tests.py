from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.utils import timezone
from selenium import webdriver

from .models import Status

# Create your tests here.

class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_not_exists(self):
        response = self.client.get("/status/")
        self.assertEqual(response.status_code,404)
    
    def test_url_status_page_exist(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code,200)

class TemplateStatusPageTest(TestCase):
    def test_html_status_page_exist(self):
        response = self.client.get("/")
        self.assertTemplateUsed(response,'story6/index.html')
    
    def test_html_halo_apa_kabar_exist(self):
        response = self.client.get("/")
        self.assertIn("Halo, apa kabar?", response.content.decode())

class ViewsTest(TestCase):
    def test_form_is_valid(self):
        response = self.client.post("/", follow=True, data={
            'status_message': 'Lulus Pepew Aamiin',
        })

        self.assertRedirects(response, "/")
        self.assertEqual(Status.objects.count(),1)
    
    def test_form_is_valid_2(self):
        response = self.client.post("/", follow=True, data={
            'status_message': 'a' * 302,
        })

        self.assertEqual(Status.objects.count(),0)

    def test_show_status(self):
        response = self.client.post('/', follow=True, data={
            'status_message': 'Lulus Pepew Aamiin',
        })

        self.assertContains(response, 'Lulus Pepew Aamiin', html=True)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium_temp = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')

    def tearDown(self):
        self.selenium_temp.quit()
        super().tearDown()

    def test_status_page(self):
        self.selenium_temp.get(self.live_server_url + '/')

        status = self.selenium_temp.find_element_by_id('id_status_message')
        status.send_keys('Coba Coba')
        status.submit()

        self.assertInHTML('Coba Coba', self.selenium_temp.page_source)

# Generated by Django 2.2.6 on 2019-11-04 13:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('story6', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='status',
            old_name='status',
            new_name='status_message',
        ),
    ]

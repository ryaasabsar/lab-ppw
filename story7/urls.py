from django.urls import path, include
from .views import story7_index

app_name = "story7"

urlpatterns = [
    path('', story7_index, name='story7_index'),
]
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

# Create your tests here.

class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_not_exists(self):
        response = self.client.get("/story7/story7/")
        self.assertEqual(response.status_code,404)
    
    def test_url_exists(self):
        response = self.client.get("/story7/")
        self.assertEqual(response.status_code,200)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium_temp = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')

    def tearDown(self):
        self.selenium_temp.quit()
        super().tearDown()
    
    def test_change_theme(self):
        self.selenium_temp.get(self.live_server_url + '/story7/')

        self.assertIn('id="theme-button"', self.selenium_temp.page_source)

        button = self.selenium_temp.find_element_by_id('theme-button')

        self.assertNotIn('navbar-dark', self.selenium_temp.page_source)

    def test_content_accordion_not_exist_before_clicked(self):
        self.selenium_temp.get(self.live_server_url+"/")
        self.assertNotIn('Currently doing something big!', self.selenium_temp.page_source)

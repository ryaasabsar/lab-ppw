(function() {
$('#theme-button').click(function() {
    if ($(this).is(':checked')) {
        $('nav').removeClass('b-white navbar-light')
        $('nav').addClass('b-black navbar-dark')
        $('div .card').removeClass('b-black')
        $('div .card').addClass('b-white')
        $('.bg-changed1').removeClass('black-background')
        $('.bg-changed1').addClass('white-background')
        $('.bg-changed2').removeClass('white-background')
        $('.bg-changed2').addClass('black-background')
        $('.b-changed-accordion').removeClass('b-white')
        $('.b-changed-accordion').addClass('b-black')
    } else {
        $('nav').removeClass('b-black navbar-dark')
        $('nav').addClass('b-white navbar-light')
        $('div .card').removeClass('b-white')
        $('div .card').addClass('b-black')
        $('.bg-changed2').removeClass('black-background')
        $('.bg-changed2').addClass('white-background')
        $('.bg-changed1').removeClass('white-background')
        $('.bg-changed1').addClass('black-background')
        $('.b-changed-accordion').removeClass('b-black')
        $('.b-changed-accordion').addClass('b-white')
    }
});
})();
